This a basic SpringBoot application for demo purposes.

To build 
```
mvn package
```

To run 
```
java -jar <pathToJar>/DemoApp.jar
```

To validate if it works open http://localhost:8080 ;)