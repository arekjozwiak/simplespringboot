package it.devnotes.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicMvcApplication.class, args);
    }

}
