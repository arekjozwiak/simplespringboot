package it.devnotes.demo.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartItem {

    private Long id;
    private String name;
    private Double price;
    private Double quantity;

}
