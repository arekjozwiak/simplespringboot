package it.devnotes.demo.service;

import it.devnotes.demo.model.CartItem;

import java.util.List;

public interface ShoppingListService {

    void addItem(CartItem item);
    void deleteItem(CartItem item);
    void clearList();
    List<CartItem> getAllShoppingListItems();

}
